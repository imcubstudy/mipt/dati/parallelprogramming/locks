#include "../lock.h"

#include <cstdint>
#include <stdexcept>
#include <atomic>
#include <thread>

class TTAS_memorder final: public Lock
{
public:
    virtual void lock() override
    {
        do {
            // Test
            while(m_state.locked.load(std::memory_order::memory_order_relaxed)) {
                continue;
            }
            // Test-And-Set
        } while(m_state.locked.exchange(true, std::memory_order::memory_order_acquire));
    }

    virtual void unlock() noexcept override
    {
        m_state.locked.store(false, std::memory_order::memory_order_release);
    }

    TTAS_memorder():
        m_state{}
    {
        m_state.locked.store(false, std::memory_order::memory_order_seq_cst);
    }

    ~TTAS_memorder() override
    {
        if(m_state.locked.load(std::memory_order::memory_order_seq_cst)) {
            throw std::runtime_error("Attemt to destruct currently held lock");
        }
    }

private:
    struct alignas(128) {
        union {
            std::atomic<bool> locked;
            uint8_t pad__[128];
        };
    } m_state;
};
