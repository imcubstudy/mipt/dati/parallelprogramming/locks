#include "../lock.h"

#include <cstdint>
#include <stdexcept>
#include <atomic>

class TTAS_Simple final: public Lock
{
public:
    virtual void lock() override
    {
        do {
            // Test
            while(m_state.locked.load(std::memory_order::memory_order_seq_cst)) {
                continue;
            }
            // Test-And-Set
        } while(m_state.locked.exchange(true));
    }

    virtual void unlock() noexcept override
    {
        m_state.locked.store(false, std::memory_order::memory_order_seq_cst);
    }

    TTAS_Simple():
        m_state{}
    {
        m_state.locked.store(false, std::memory_order::memory_order_seq_cst);
    }

    ~TTAS_Simple() override
    {
        if(m_state.locked.load(std::memory_order::memory_order_seq_cst)) {
            throw std::runtime_error("Attemt to destruct currently held lock");
        }
    }

private:
    struct alignas(128) {
        union {
            std::atomic<bool> locked;
            uint8_t pad__[128];
        };
    } m_state;
};
