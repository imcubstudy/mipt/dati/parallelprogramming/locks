#include "../lock.h"

#include <cstdint>
#include <stdexcept>
#include <atomic>
#include <thread>

class Ticket_yeild final: public Lock
{
public:
    virtual void lock() override
    {
        uint32_t my_ticket = m_state.atomics.next_ticket.fetch_add(1, std::memory_order::memory_order_seq_cst);
        while(my_ticket != m_state.atomics.now_serving.load(std::memory_order_acquire)) {
            std::this_thread::yield();
        }
    }

    virtual void unlock() noexcept override
    {
        m_state.atomics.now_serving.fetch_add(1, std::memory_order::memory_order_release);
    }

    Ticket_yeild():
        m_state{}
    {
        m_state.atomics.now_serving.store(0, std::memory_order::memory_order_seq_cst);
        m_state.atomics.next_ticket.store(0, std::memory_order::memory_order_seq_cst);
    }

    ~Ticket_yeild() override
    {}

private:
    union alignas(128){
        struct {
            std::atomic<uint32_t> now_serving;
            std::atomic<uint32_t> next_ticket;
        } atomics;
        uint8_t pad__[128];
    } m_state;
};
