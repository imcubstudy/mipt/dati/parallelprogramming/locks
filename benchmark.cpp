#include "lock.h"
#include "locks/ttas_simple.h"
#include "locks/ttas_memorder.h"
#include "locks/ttas_yeild.h"
#include "locks/ticket_yeild.h"

#include <thread>
#include <vector>
#include <iostream>
#include <chrono>
#include <memory>
#include <map>
#include <string>
#include <fstream>

class Timer
{
public:
    static void start()
    { m_start = std::chrono::system_clock::now(); }

    static auto stop()
    {
        m_stop = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(m_stop - m_start);
        return duration.count();
    }
private:
    static std::chrono::time_point<std::chrono::system_clock> m_start;
    static std::chrono::time_point<std::chrono::system_clock> m_stop;
};
std::chrono::time_point<std::chrono::system_clock> Timer::m_start = std::chrono::system_clock::now();
std::chrono::time_point<std::chrono::system_clock> Timer::m_stop = std::chrono::system_clock::now();

const size_t maxThreads = 50;
const size_t incTarget = 100000;
const size_t nAverage = 15;
const size_t heavyWorkDuration = 10;

int main()
{
    size_t incVar = 0;
    std::unique_ptr<Lock> lock;

    auto threadRoutine = [&lock, &incVar]() -> void {
        bool flag_done = false;
        while(!flag_done) {
            lock->lock();
            if(incVar < incTarget) {
                ++incVar;
            } else {
                flag_done = true;
            }
            lock->unlock();
        }
    };

    auto launchThreads = [&](const size_t nThreads) -> size_t {
        std::vector<std::thread> threads(nThreads);
        Timer::start();
        incVar = 0;
        for(auto& t: threads) {
            t = std::thread(threadRoutine);
        }
        for(auto& t: threads) {
            t.join();
        }
        if(incVar != incTarget) {
            throw std::runtime_error("Inc failed");
        }
        return Timer::stop();
    };

    auto threadRoutineHeavy = [&lock]() -> void {
        bool flag_done = false;
        lock->lock();
        auto start = std::chrono::system_clock::now();
        while(!flag_done) {
            auto now = std::chrono::system_clock::now();
            if(std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count() >= heavyWorkDuration){
                flag_done = true;
            }
        }
        lock->unlock();
    };

    auto launchThreadsHeavy = [&](const size_t nThreads) -> size_t {
        std::vector<std::thread> threads(nThreads);
        Timer::start();
        for(auto& t: threads) {
            t = std::thread(threadRoutineHeavy);
        }
        for(auto& t: threads) {
            t.join();
        }
        return Timer::stop();
    };

    auto measure = [&](const size_t nThreads) -> size_t {
        auto avg = 0u;
        for(auto i = 0u; i < nAverage; ++i) {
            avg += launchThreads(nThreads);
        }
        avg /= nAverage;
        return avg;
    };

    auto measureHeavy = [&](const size_t nThreads) -> size_t {
        auto avg = 0u;
        for(auto i = 0u; i < nAverage; ++i) {
            avg += launchThreadsHeavy(nThreads);
        }
        avg /= nAverage;
        return avg;
    };

    std::map<std::string,
        std::map<std::string, std::array<size_t, maxThreads>>
    > results;

    // lock = std::make_unique<TTAS_Simple>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ttas_simple light: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ttas_simple"]["light"][nThreads - 1] = measure(nThreads);
    //     std::cout << results["ttas_simple"]["light"][nThreads - 1] << "ms." << std::endl;
    // }

    // lock = std::make_unique<TTAS_memorder>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ttas_memorder light: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ttas_memorder"]["light"][nThreads - 1] = measure(nThreads);
    //     std::cout << results["ttas_memorder"]["light"][nThreads - 1] << "ms." << std::endl;
    // }

    // lock = std::make_unique<TTAS_yeild>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ttas_yeild light: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ttas_yeild"]["light"][nThreads - 1] = measure(nThreads);
    //     std::cout << results["ttas_yeild"]["light"][nThreads - 1] << "ms." << std::endl;
    // }

    // lock = std::make_unique<Ticket_yeild>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ticket_yeild light: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ticket_yeild"]["light"][nThreads - 1] = measure(nThreads);
    //     std::cout << results["ticket_yeild"]["light"][nThreads - 1] << "ms." << std::endl;
    // }

    // lock = std::make_unique<TTAS_Simple>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ttas_simple heavy: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ttas_simple"]["heavy"][nThreads - 1] = measureHeavy(nThreads);
    //     std::cout << results["ttas_simple"]["heavy"][nThreads - 1] << "ms." << std::endl;
    // }

    // lock = std::make_unique<TTAS_memorder>();
    // for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
    //     std::cout << "Measuring ttas_memorder heavy: nThreads[" << nThreads << "] time: " << std::flush;
    //     results["ttas_memorder"]["heavy"][nThreads - 1] = measureHeavy(nThreads);
    //     std::cout << results["ttas_memorder"]["heavy"][nThreads - 1] << "ms." << std::endl;
    // }

    lock = std::make_unique<TTAS_yeild>();
    for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
        std::cout << "Measuring ttas_yaeild heavy: nThreads[" << nThreads << "] time: " << std::flush;
        results["ttas_yeild"]["heavy"][nThreads - 1] = measureHeavy(nThreads);
        std::cout << results["ttas_yeild"]["heavy"][nThreads - 1] << "ms." << std::endl;
    }

    lock = std::make_unique<Ticket_yeild>();
    for(auto nThreads = 1u; nThreads <= maxThreads; ++nThreads) {
        std::cout << "Measuring ticket_yeild heavy: nThreads[" << nThreads << "] time: " << std::flush;
        results["ticket_yeild"]["heavy"][nThreads - 1] = measureHeavy(nThreads);
        std::cout << results["ticket_yeild"]["heavy"][nThreads - 1] << "ms." << std::endl;
    }

    std::vector<std::string> locks = {
        // "ttas_simple",
        // "ttas_memorder",
        "ttas_yeild",
        "ticket_yeild"
    };

    std::vector<std::string> workloads = {
        // "light",
        "heavy"
    };

    for(auto& l: locks) {
        for(auto& w: workloads) {
            std::ofstream out(l + "_" + w);
            for(auto i = 0u; i < maxThreads; ++i) {
                out << i + 1 << " " << results[l][w][i] << "\n";
            }
        }
    }

    return 0;
}