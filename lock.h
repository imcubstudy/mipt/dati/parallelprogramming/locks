#ifndef LOCK_H__
#define LOCK_H__

class Lock
{
public:
    virtual void lock() = 0;
    virtual void unlock() noexcept = 0;

    virtual ~Lock() {};
};

#endif // LOCK_H__